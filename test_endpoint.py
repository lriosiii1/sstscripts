import powerschool
import pprint as pp
import json

# Python 2

client_id = 'e1a03825-1d4e-4619-a850-19696230cb57'
client_secret = '1dee60d2-7b5e-4012-acad-b18070b3bf12'
base_url = 'https://iredellcharteracad.powerschool.com'
named_query = 'com.csusa.launchpad.students.active_students'

# optional
access_token = '8d74e4c7-e8ad-48dd-9c08-73af2e16f421'
payload = '{"$q": "STUDENTS.DCID==1873"}'

# student_payload = {
#   "students": {
#     "extensions": "studentcorefields",
#     "student": {
#       "local_id": "200393755",
#       "client_uid": "3",
#       "action": "INSERT",
#       "demographics": {
#         "birth_date": "2012-03-21",
#         "gender": "M"
#       },
#       "name": {
#         "first_name": "Daniel",
#         "last_name": "Alvarez",
#         "middle_name": ""
#       },
#       "school_enrollment": {
#         "grade_level": 4,
#         "enroll_status": "P",
#         "entry_date": "2018-08-10",
#         "exit_date": "2019-05-24",
#         "school_number": "7803",
#         "entry_code": "E01"
#       }
#     }
#   }
# }

#     #'{"$q": "students.u_def_ext_students.cleverpassword==\u0000", "projection": "students.grade_level;students.schoolid;"}'
# dataversion = 228919000
# applicationname = "Students"
# delta_payload = '{"$dataversion": "48", "$dataversion_applicationname": "Students"}'

# instantiate ps_server
ps_server = powerschool.PS_Instance(client_id=client_id, client_secret=client_secret, base_url=base_url)

# get ps_server token
ps_server.get_token()
print(ps_server.access_token)

# empty endpoint post
returned_data = ps_server.post_endpoint(named_query=named_query, access_token=access_token)

# student post
# returned_data = ps_server.post_student(access_token=access_token, payload=student_payload)

# delta post
# response_get = ps_server.post_delta(access_token=access_token, dataversion=dataversion, application_name=applicationname, payload=delta_payload)


pp.pprint(returned_data)



