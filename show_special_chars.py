import string
import xlsxwriter

def has_special(record):
	try:
		record.encode(encoding='utf-8').decode('ascii')
	except UnicodeDecodeError:
		return True
	else:
		return False


def mark_error(field):
	return 'ERROR:' + field

workbook = xlsxwriter.Workbook('the_workbook.xlsx')
sheet1 = workbook.add_worksheet('sheet1')
bold = workbook.add_format({'bold': True})

# open file
with open('PowerSchoolStudentExport(11).txt', 'r') as student_file:
	clean_lines = list()
	dirty_lines = list()
	# read each row
	lines = student_file.read()
	data = lines.splitlines()
	for line in data:
		# check if row has special character
		if has_special(line):
			for field in line.split('\t'):
				if has_special(field):
					line = line.replace(field, mark_error(field))
					dirty_lines.append(line + '\n')
		else:
			clean_lines.append(line + '\n')

with open('cleans.txt', 'w+') as clean_outfile:
	for line in clean_lines:
		clean_outfile.write(line)

with open('dirtys.txt', 'w+') as dirty_outfile:
	for line in dirty_lines:
		dirty_outfile.write(line)

#for row_num, line in enumerate(clean_lines, 0):
#	sheet1.write(row_num, 0, line)

workbook.close()