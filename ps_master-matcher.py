import csv
import logging
from datetime import datetime


# GCHS
master_file = open('master_smca.csv', 'r')
master_data = csv.reader(master_file)


ps_file = open('ps_smca.csv', 'r', encoding='latin-1')
ps_data = csv.reader(ps_file)

master_data_list = list()
ps_data_list = list()


for master_record in master_data:
	master_data_list.append(master_record)
for ps_record in ps_data:
	ps_data_list.append(ps_record)


count = 0
matches_list_output = list()
matches = list()
non_matches_list = list()


for master_record in master_data_list:
	for ps_record in ps_data_list:
		# if same firstlast name
		if master_record[1].lower() + master_record[2].lower() == ps_record[4].lower() + ps_record[6].lower():
			# if same school
			if master_record[4] == ps_record[10]:
				matches_list_output.append(list((ps_record[2], master_record[1], master_record[2], master_record[3],master_record[4], master_record[5], master_record[6], master_record[7],master_record[8], master_record[9], master_record[10], master_record[11], master_record[12])))
				matches.append(master_record)
				count += 1
				"""
				# if same dob
				try:
					if datetime.strptime(master_record[10], '%m/%d/%Y') == datetime.strptime(ps_record[5], '%m/%d/%y'):
						matches_list_output.append(list((ps_record[2], master_record[1], master_record[2], master_record[3], master_record[4],master_record[5], master_record[6], master_record[7], master_record[8],master_record[9], master_record[10],master_record[11], master_record[12])))
						matches.append(master_record)
						count += 1
				except ValueError:
					try:
						if datetime.strptime(master_record[10], '%m/%d/%y') == datetime.strptime(ps_record[5],'%m/%d/%y'):
							matches_list_output.append(list((ps_record[2], master_record[1], master_record[2], master_record[3], master_record[4], master_record[5],master_record[6], master_record[7], master_record[8],master_record[9], master_record[10], master_record[11], master_record[12])))
							matches.append(master_record)
							count += 1
					except Exception:
						try:
							if datetime.strptime(master_record[10], '%b %d, %Y') == datetime.strptime(ps_record[5],'%m/%d/%y'):
								matches_list_output.append(list((ps_record[2], master_record[1], master_record[2],master_record[3], master_record[4], master_record[5],master_record[6], master_record[7], master_record[8],master_record[9], master_record[10], master_record[11],master_record[12])))
								matches.append(master_record)
								count += 1
						except Exception as e:
							non_matches_list.append(master_record)
							print(master_record)
							logging.error(str(e))
				"""

	if master_record not in matches:
		non_matches_list.append(master_record)

print(count)

outfile = open('psmatched_' + master_file.name, 'w')
writer = csv.writer(outfile, delimiter=',', quotechar='"')
header = ['Universal ID', 'FIRST_NAME', 'LAST_NAME', 'MIDDLE_NAME', 'SCHOOL_ID', 'GRADE_LEVEL', 'PS_INSTACE_ID', 'SCHOOL_CODE', 'StateID', 'FSATestID', 'DOB','','FLORIDA STUDENT ID']
writer.writerow(header)
writer.writerows(matches_list_output)
writer.writerows(non_matches_list)


outfile.close()
master_file.close()
ps_file.close()

