import csv
import logging
from datetime import datetime
import os

pickup_directory = '/Users/connex12/Desktop/lp_staff_files/lp_staff_originals'
dropoff_directory = '/Users/connex12/Desktop/lp_staff_files/scrubbed'
# loop directory of staff files
for root, dirs, filenames in os.walk(pickup_directory):
	for file in filenames:
		staff_file = open(os.path.join(root, file), 'r')
		staff_data = csv.reader(staff_file)

		non_district_staff = list()
		staff_data = list(staff_data)
		original_count = len(staff_data)
		scrub_count = 0
		# remove district users by homeschoolid
		for staff_member in staff_data:
			if staff_member[1] == '0':
				scrub_count += 1
				continue
			else:
				non_district_staff.append(staff_member)

		print file
		print '  Original: ', original_count
		print '  Users removed: ', scrub_count

		os.chdir(dropoff_directory)
		outfile = open(datetime.today().strftime("%m%d%Y") + file, 'w+')
		writer = csv.writer(outfile, delimiter=',', quotechar='"')
		writer.writerows(non_district_staff)

		staff_file.close()
		outfile.close()
