import base64
import requests
import json


class PS_Instance:
	""" powerschool instance """

	def __init__(self, client_id, client_secret, base_url, access_token=None):
		self.client_id = client_id
		self.client_secret = client_secret
		self.base_url = base_url
		self.access_token = access_token

	# Make a new token
	def get_token(self):
		encoded_string = base64.urlsafe_b64encode(self.client_id + ':' + self.client_secret)
		header = {
			'Authorization': 'Basic ' + encoded_string,
			'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
		}

		response = requests.post(self.base_url + '/oauth/access_token/', data='grant_type=client_credentials', headers=header)
		response = response.json()
		self.access_token = response['access_token']
		return response

	# pull endpoint data from named query
	def post_endpoint(self, named_query, access_token, payload=None):
		header_parameters = {
			'Authorization': 'Bearer ' + access_token,
			'Accept': 'application/json', 'Content-Type': 'application/json'}
		full_endpoint = self.base_url + '/ws/schema/query/' + named_query
		endpoint_response = requests.post(url=full_endpoint, headers=header_parameters, data=payload)
		json_resp = endpoint_response.json()
		print(full_endpoint)
		return json_resp

	def post_student(self, access_token, payload=None):
		header_parameters = {
			'Authorization': 'Bearer ' + access_token,
			'Accept': 'application/json', 'Content-Type': 'application/json'}
		full_endpoint = self.base_url + '/ws/v1/student'
		print(full_endpoint)
		endpoint_response = requests.post(url=full_endpoint, headers=header_parameters, data=payload)
		json_resp = endpoint_response.json()
		return json_resp

	# delta
	def post_delta(self, access_token, application_name, dataversion, payload):
		header_parameters = {
			'Authorization': 'Bearer ' + access_token,
			'Accept': 'application/json', 'Content-Type': 'application/json'}
		full_endpoint = self.base_url + '/ws/dataversion/' + application_name + '/' + str(dataversion)
		endpoint_response = requests.get(url=full_endpoint, headers=header_parameters, data=payload)
		json_response = endpoint_response.json()
		return json_response





